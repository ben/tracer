# tracer

ported to py3 from [selfsame/tracer](https://notabug.org/selfsame/tracer)

irc tracery bot

deps:
* https://github.com/aparrish/pytracery

### setup

1. install deps
    virtualenv -p python3 venv
    . venv/bin/activate
    pip install -r requirements.txt

1. adjust config.ini
1. start the bot
    venv/bin/python3 tracer.py

### daemonization

1. adjust tracer.service
1. `mkdir -p ~/.config/systemd/user`
1. `cp tracer.service ~/.config/systemd/user/`
1. `systemctl --user daemon-reload`
1. `systemctl --user enable --now tracer`

